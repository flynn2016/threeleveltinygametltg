﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact_npc : Interactable {


    public override void Interact()
    {
        base.Interact();

        Interacting();
    }

    void Interacting()
    {
        DialogueSystem.instance.InvokeDialogue(Player_Controller_2D.instance.transform, transform, 1);
    }
}
