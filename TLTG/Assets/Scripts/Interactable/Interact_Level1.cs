﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interact_Level1 : Interactable
{

    public override void Interact()
    {
        base.Interact();

        Interacting();
    }

    void Interacting()
    {
        switch (GameController.instance.storyline)
        {
            case 1:  // 开始 可以和dark后羿对话
                DialogueSystem.instance.InvokeDialogue(Player_Controller_2D.instance.transform, transform, 2);
                break;
            case 2: // 和dark后羿说完话 未解锁石碑
                break;
            case 3: // 解锁了石碑 可以和dark后羿对话
                DialogueSystem.instance.InvokeDialogue(Player_Controller_2D.instance.transform, transform, 3);
                break;
            case 4: //和dark后羿话毕，可以和light后羿对话
                DialogueSystem.instance.InvokeDialogue(Player_Controller_2D.instance.transform, transform, 4);
                break;
            case 5: //拿起羽毛并过关
                GameController.instance.Feather.SetActive(false);
                GameController.instance.feather_UI.sprite = GameController.instance.feather_obtained;
                // Level Passed notification

                GameController.instance.Hint.GetComponentInChildren<Text>().text = "恭喜你通关啦！";
                GameController.instance.Hint.SetActive(true);
                break;
            default:
                break;
        }
    }
}
