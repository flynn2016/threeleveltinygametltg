﻿using UnityEngine;

public class Interact_shibei : Interactable {

    public string Bei_Order;

    public Sprite Bei_lit;
    public Sprite Bei_dark;

    public override void Interact()
    {
        base.Interact();

        Interacting();
    }

    void Interacting()
    {
        if (!hasInteracted)
        {
            GetComponent<SpriteRenderer>().sprite = Bei_lit;
            GameController.instance.Bei_Order += name.Substring(4); // return 石碑 order
            Debug.Log("石碑 order: "+GameController.instance.Bei_Order);
        }
    }
}
