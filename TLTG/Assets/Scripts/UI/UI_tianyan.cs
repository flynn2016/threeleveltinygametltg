﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UI_tianyan : MonoBehaviour, IPointerClickHandler
{

    public Sprite Dark_eye;
    public Sprite Light_eye;
    public Sprite Red_eye;


    public void OnPointerClick(PointerEventData pointerEventData)
    {
        GameController.instance.world_changed = true;

        if (GameController.instance.World_light)
        {
            Camera.main.transform.position = new Vector3(0.0f, 14.5f, -10.0f);
            GameController.instance.World_light = false;
            this.GetComponent<Image>().sprite = Dark_eye;
        }
        else
        {
            Camera.main.transform.position = new Vector3(0.0f, 1.0f, -10.0f);
            GameController.instance.World_light = true;
            this.GetComponent<Image>().sprite = Light_eye;
        }
    }
}
