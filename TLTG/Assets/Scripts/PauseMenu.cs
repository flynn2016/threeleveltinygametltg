﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    //public GameObject Menu;

    void Start()
    {
        gameObject.SetActive(false);
    }

    public void BackToGame()
    {
        gameObject.SetActive(false);
    }

    public void BackToMain()
    {
        SceneManager.LoadScene("Menu");
    }

    public void OpenPause()
    {
        gameObject.SetActive(true);
    }

}
