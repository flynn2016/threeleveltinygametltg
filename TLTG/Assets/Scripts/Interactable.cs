﻿using UnityEngine;

public class Interactable : MonoBehaviour {

    public float radius = 2f; // interact distance
    public bool hasInteracted = false;
    public Transform interactionTransform;
    public Camera cam;

    public virtual void Interact()
    {
        // This method is meant to be overwritten 
    }

    void Start()
    {
        cam = Camera.main;
    }

    void OnDrawGizmosSelected()
    {
        if (interactionTransform == null)
            interactionTransform = transform;

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTransform.position, radius);
    }
}
