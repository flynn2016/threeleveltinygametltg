﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public static class SaveLoadManager  {

	public static void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream stream = new FileStream(Application.persistentDataPath + "/save.dat",FileMode.Create);

        GameState data = new GameState();
        bf.Serialize(stream, data);
        stream.Close();
    }

    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/save.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream stream = new FileStream(Application.persistentDataPath + "/save.dat", FileMode.Open);

            GameState data = bf.Deserialize(stream) as GameState;
            Debug.Log(data.Level_1_state);
            stream.Close();
            GlobalControl.Instance.Moon = data.Level_1_state;
            GlobalControl.Instance.Sun = data.Level_2_state;
            GlobalControl.Instance.Star = data.Level_3_state;
            Debug.Log("wtf"+data.Level_1_state);
        }
    }
}


[Serializable]
public class GameState
{
    public bool Level_1_state;
    public bool Level_2_state;
    public bool Level_3_state;

    public GameState()
    {
        Level_1_state = GlobalControl.Instance.Moon;
        Level_2_state = GlobalControl.Instance.Sun;
        Level_3_state = GlobalControl.Instance.Star;
    }
}
