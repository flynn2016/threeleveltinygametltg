﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class TestController : MonoBehaviour {
    public Text test_text;
	// Use this for initialization
	void Start () {
        SaveLoadManager.Load();
        UpdateText();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ToMoon()
    {
        SceneManager.LoadScene("1_Moon");
    }

    public void ToSun()
    {
        SceneManager.LoadScene("2_Sun");
    }

    public void ToStar()
    {
        SceneManager.LoadScene("3_Star");
    }

    public void UpdateText()
    {
        test_text.text = "Moon finished:  " + GlobalControl.Instance.Moon+"  Sun finished:  "+GlobalControl.Instance.Sun+ "  Star finished: " + GlobalControl.Instance.Star;
    }

    public void FinishMoon()
    {
        GlobalControl.Instance.Moon = !GlobalControl.Instance.Moon;
        SaveLoadManager.Save();
    }
    public void FinishSun()
    {
        GlobalControl.Instance.Sun = !GlobalControl.Instance.Sun;
        SaveLoadManager.Save();
    }

    public void FinishStar()
    {
        GlobalControl.Instance.Star = !GlobalControl.Instance.Star;
        SaveLoadManager.Save();
    }



}
