﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

    public GameObject MainMenu;
    public GameObject LevelSelection;

    void Start()
    {
        MainMenu.SetActive(true);
        LevelSelection.SetActive(false);
    }

    public void Play()
    {
        MainMenu.SetActive(false);
        LevelSelection.SetActive(true);
    }

    public void Level1()
    {
        SceneManager.LoadScene("FuSang");
    }

    public void Back()
    {
        MainMenu.SetActive(true);
        LevelSelection.SetActive(false);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
