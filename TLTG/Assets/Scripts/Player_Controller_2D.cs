﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player_Controller_2D : MonoBehaviour {

    //Main Cam
    public Camera cam;

    //Click to Move
    private Vector2 newPosition;
    public float PlayerSpeed = 0.07f;

    //Raycast
    private bool Interacting;
    public bool conversating = false;
    private Interactable temp;
    public bool houyi_ed;

    #region     //singleton
    public static Player_Controller_2D instance;
    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("more than one instance");
            return;
        }
        instance = this;
    }
    #endregion

    void Start()
    {
        cam = Camera.main;
        newPosition = cam.WorldToScreenPoint(gameObject.transform.position);
    }
    void Update()
    {
        PlayerControl(); //Control player movement
        RaycastDetect(); //detect raycast
    }

    private void PlayerControl()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!EventSystem.current.IsPointerOverGameObject()){

                newPosition = Input.mousePosition;
            }

        }
        if (!Interacting)
        {
            if ((cam.WorldToScreenPoint(transform.position).x - newPosition.x) < -3)
            {
                this.transform.position = new Vector3(transform.position.x + PlayerSpeed, transform.position.y, transform.position.z);
            }

            else if ((cam.WorldToScreenPoint(gameObject.transform.position).x - newPosition.x) > 3)
            {
                this.transform.position = new Vector3(transform.position.x - PlayerSpeed, transform.position.y, transform.position.z);
            }
        }
    }

    public void DisablePlayer()
    {
        Interacting = true;
    }

    public void RestorePlayer()
    {
        Interacting = false;
    }

    public void RaycastDetect()// TODO: better ways to implement this code block ???
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (conversating)  // allow interaction with npc done without click npc
            {
                temp.Interact();
            }
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {

                Interactable interactable = hit.collider.GetComponent<Interactable>();
                if (interactable != null)
                {
                    if (hit.collider.GetComponent<Interact_shibei>())  // Interact with shibei
                    {
                        if (!hit.collider.GetComponent<Interactable>().hasInteracted)
                        {
                            // If we are close enough
                            if (Distance(hit.transform,hit.transform.GetComponent<Interactable>().radius))
                            {
                                // Interact with the object
                                hit.collider.GetComponent<Interact_shibei>().Interact();
                                hit.collider.GetComponent<Interactable>().hasInteracted = true;
                            }
                        }
                    }

                    if (hit.collider.GetComponent<Interact_npc>())  // Interact with npc
                    {
                        // If we are close enough
                        if (Distance(hit.transform, hit.transform.GetComponent<Interactable>().radius)&&!conversating)
                        {
                            // Interact with the object
                            conversating = true;
                            temp = hit.collider.GetComponent<Interactable>();
                            hit.collider.GetComponent<Interact_npc>().Interact();
                            hit.collider.GetComponent<Interactable>().hasInteracted = true;
                        }

                    }

                    if (hit.collider.GetComponent<Interact_Level1>())  // Interact with npc
                    {

                        // If we are close enough
                        if (Distance(hit.transform, hit.transform.GetComponent<Interactable>().radius))
                        {
                            // Interact with the object
                            if (hit.transform.name == "HouYi"&&!houyi_ed|| hit.transform.name == "HouYi_Light")
                            {
                                houyi_ed = true;
                                conversating = true;
                            }
                            temp = hit.collider.GetComponent<Interactable>();
                            hit.collider.GetComponent<Interact_Level1>().Interact();
                            hit.collider.GetComponent<Interactable>().hasInteracted = true;
                        }
                    }


                }
            }

        }
    }

    private bool Distance(Transform target, float radius) // helper function calculate distance between player and interacting object
    {
        if (Vector3.Distance(Player_Controller_2D.instance.transform.position, target.transform.position) <= radius)
            return true;
        else
            return false;
    }

}
