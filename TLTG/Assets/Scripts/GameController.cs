﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    //General Use
    float gameTimer = 0.0f;
    public Camera cam;


    #region // variables and references
    //World Logic varaibles
    public bool World_light = true;
    public bool world_changed = false;

    public string Bei_Order;
    public bool houyi_keep=false;
    public bool shibei_solved = false;
    public bool feather_falling = false;
    public bool level_cleared;
    public bool houyi_showup = false;

    public int storyline;
    

    //All GameObjects reference
    public GameObject[] Dark_Bei = new GameObject[4];
    public GameObject[] Light_Bei = new GameObject[4];
    public Sprite Bei_Unlit;
    public GameObject Dark_HouYi;
    public GameObject Light_JinWu;
    public GameObject Houyi_Light;
    public GameObject River;
    public GameObject JinWu;
    public GameObject Feather;

    //Temp GameObject - just for play testing
    public GameObject Hint;

    //All UI
    public Image feather_UI;
    public Image Tianyan_UI;
    public Sprite feather_obtained;
    public Sprite Highlighted_tianyan;

    public GameObject PauseMenu;
    #endregion

    #region     //singleton

    public static GameController instance = null;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.LogWarning("more than one instance");
            return;
        }
        instance = this;
    }
    #endregion

    void Start () {
        cam = Camera.main;
        River.SetActive(false);
        world_changed = false;
        feather_falling = false;
}
	
	void Update () {
        Darkworld_Events();       //暗世界events
        Check_BeiOrder();        //check 石碑
        Feather_Fall();          //let feather fall
    }



    void Darkworld_Events()
    {
        if(!PauseMenu.activeSelf){
            gameTimer += Time.deltaTime;
        }

        int seconds = (int)gameTimer % 60;

        if(seconds >= 5 && !world_changed)
        {
            Tianyan_UI.sprite = Highlighted_tianyan;
        }

        

        // Dark世界每隔五秒熄灭一个石碑
        if (seconds >= 0 && seconds < 5)
        {
            Dark_Bei[0].GetComponent<SpriteRenderer>().sprite = Bei_Unlit;
        }
        else if (seconds >= 5 && seconds < 10)
        {
            Dark_Bei[2].GetComponent<SpriteRenderer>().sprite = Bei_Unlit;
        }
        else if (seconds >= 10 && seconds < 15)
        {
            Dark_Bei[3].GetComponent<SpriteRenderer>().sprite = Bei_Unlit;
        }
        else if (seconds >= 15)
        {
            Dark_Bei[1].GetComponent<SpriteRenderer>().sprite = Bei_Unlit;
        }

        // Dark世界后羿出没
        if (seconds == 5&&!Dark_HouYi.activeSelf)
        {
            houyi_showup = true;
            Dark_HouYi.SetActive(true);
        }
        if (seconds == 8 && !houyi_keep)
        {
            Dark_HouYi.SetActive(false);
        }

        // Light世界金乌飞走
        if (seconds >= 50)
        {
            Light_JinWu.SetActive(false);
        }

        if(seconds >= 59)
        {
            SceneManager.LoadScene("FuSang");
        }
    }

    void Check_BeiOrder()
    {
        if (Bei_Order .Length == 4)
        {
            if (Bei_Order == "2431")
            {
                Debug.Log("连接光明与黑暗的河水出来吧！");
                Bei_Order = "";
                River.SetActive(true);
                //Temp - just for play testing
                StartCoroutine(ShowHint());
                shibei_solved = true;
                storyline = 3;
                Player_Controller_2D.instance.houyi_ed = false;// reset houyi conversating;
            }
            else
            {
                for(int i = 0; i < Light_Bei.Length; i++)
                {
                    Light_Bei[i].GetComponent<Interactable>().hasInteracted = false;
                    Light_Bei[i].GetComponent<SpriteRenderer>().sprite = Bei_Unlit;
                }
                Bei_Order = "";
            }
        }
    }

    //Temp function, just for play testing
    IEnumerator ShowHint()
    {
        Hint.SetActive(true);
        yield return new WaitForSeconds(3);
        Hint.SetActive(false);
    }

    void Feather_Fall()
    {
        if (feather_falling)
        {
            Feather.transform.position = new Vector3(Feather.transform.position.x, Feather.transform.position.y-0.05f, Feather.transform.position.z);
        }

        if (Feather.transform.position.y <= -2.5f)
        {
            feather_falling = false;
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene("FuSang");
    }
}
