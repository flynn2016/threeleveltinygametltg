﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueSystem : MonoBehaviour
{
    //variables
    int sprite_index = 0;
    bool npc_talking;
    //reference
    public Transform dialogue_tudi;
    public Transform dialogue_player;
    public Transform dialogue_houyi;
    public Sprite[] tudi_sprite = new Sprite[3];
    public Sprite houyi_sprite_wait;
    public Sprite houyi_sprite_water;


    #region//singleton
    public static DialogueSystem instance;
    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("more than one instance");
            return;
        }
        instance = this;
    }
    #endregion


    public void InvokeDialogue(Transform player, Transform target, int NPC)
    {

        Player_Controller_2D.instance.DisablePlayer();
        switch (NPC)
        {
            case 1:
                StartConversation(dialogue_tudi.GetComponent<SpriteRenderer>(), dialogue_player.GetComponent<SpriteRenderer>(), 1, 3,true);
                break;
            case 2:
                StartConversation(dialogue_houyi.GetComponent<SpriteRenderer>(), dialogue_player.GetComponent<SpriteRenderer>(), 2, 1,false);
                break;
            case 3:
                StartConversation(dialogue_houyi.GetComponent<SpriteRenderer>(), dialogue_player.GetComponent<SpriteRenderer>(), 3, 1,false);
                break;
            case 4:
                StartConversation(dialogue_houyi.GetComponent<SpriteRenderer>(), dialogue_player.GetComponent<SpriteRenderer>(), 4, 1, false);
                break;
            default:
                break;
        }

       
    }

    public void StartConversation(SpriteRenderer Target, SpriteRenderer Player,int npc_index,int sprite_length,bool npc_first) //TODO: hard coded here make it more general
    {
        if (sprite_index == 0)
        {
            npc_talking = npc_first;
        }

        if (Input.GetMouseButtonDown(0)&& sprite_index<sprite_length) 
        {
            if (!npc_talking)
            {
                Target.enabled = false;
                Player.enabled = true;
                switch (npc_index)
                {
                    case 1:
                        Player.sprite = tudi_sprite[sprite_index];
                        break;
                    case 2:
                        GameController.instance.houyi_keep = true;
                        Player.sprite = houyi_sprite_wait;
                        break;
                    case 3:
                        Player.sprite = houyi_sprite_water;
                        break;
                    case 4:
                        Player.sprite = tudi_sprite[2];
                        break;
                    default:
                        break;
                }
                
            }
            else
            {
                Target.enabled = true;
                Player.enabled = false;

                switch (npc_index)
                {
                    case 1:
                        Target.sprite = tudi_sprite[sprite_index];
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
            
        }

        if (sprite_index == sprite_length)
        {
            Target.enabled = false;
            Player.enabled = false;
        }

        if (sprite_index == sprite_length+1) // end conversation
        {

            switch (npc_index)
            {
                case 1:
                    Target.transform.parent.gameObject.SetActive(false);
                    break;
                case 2:
                    GameController.instance.houyi_showup = false;
                    GameController.instance.storyline = 2;
                    break;
                case 3:
                    GameController.instance.Houyi_Light.SetActive(true);
                    GameController.instance.Dark_HouYi.SetActive(false);
                    GameController.instance.storyline = 4;
                    break;
                case 4:
                    GameController.instance.feather_falling = true;
                    GameController.instance.JinWu.SetActive(false);
                    GameController.instance.Feather.SetActive(true);
                    GameController.instance.Houyi_Light.SetActive(false);
                    GameController.instance.storyline = 5;
                    break;
            }
            Player_Controller_2D.instance.RestorePlayer();
            Player.enabled = false;
            sprite_index = 0;
            Player_Controller_2D.instance.conversating = false;
        }
        else {
            sprite_index++;
            npc_talking = !npc_talking;
        }

    }

}
